<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use App\Controller\CreateCalculatorAction;
use App\Repository\CalculatorRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CalculatorRepository::class)]
#[ApiResource(
    operations: [
        new Post(
            uriTemplate: '/calculator/calculate',
            controller: CreateCalculatorAction::class
        )
    ]
)]
class Calculator
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    #[Assert\Type('float')]
    private ?float $argument1 = null;


    #[ORM\Column]
    /**
     * @ORM\Column(type="float")
     */
    #[Assert\Type('float')]
    private ?float $argument2 = null;

    #[ORM\Column(length: 255)]
    /**
     * @ORM\Column(type="string")
     */
    private ?string $operation = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArgument1(): ?float
    {
        return $this->argument1;
    }

    public function setArgument1(float $argument1): static
    {
        $this->argument1 = $argument1;

        return $this;
    }

    public function getArgument2(): ?float
    {
        return $this->argument2;
    }

    public function setArgument2(float $argument2): static
    {
        $this->argument2 = $argument2;

        return $this;
    }

    public function getOperation(): ?string
    {
        return $this->operation;
    }

    public function setOperation(string $operation): static
    {
        $this->operation = $operation;

        return $this;
    }

    public function calculate(): float
    {
        return match ($this->operation) {
            '+' => $this->argument1 + $this->argument2,
            '-' => $this->argument1 - $this->argument2,
            '*' => $this->argument1 * $this->argument2,
            '/' => $this->argument1 / $this->argument2,
            default => throw new \InvalidArgumentException('Не корректная операция'),
        };
    }
}
