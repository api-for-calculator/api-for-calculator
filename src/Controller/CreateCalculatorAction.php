<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Calculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class CreateCalculatorAction extends AbstractController
{
    public function __construct(private SerializerInterface $serializer,)
    {
    }

    public function __invoke(Request $request): Response
    {
        $calculator = $this->serializer->deserialize($request->getContent(), Calculator::class, 'json');

        $result = $calculator->calculate();

        return $this->json(['результат калькулятора на API' => $result]);
    }
}
