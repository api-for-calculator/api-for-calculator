<?php

namespace App\Tests;

use App\Entity\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    public function testTrue(): void
    {
        $this->assertTrue(true);
    }

    public function testNotEmptySetOperation(): void
    {
        $calculate = new Calculator();
        $calculate->setOperation("+");
        $this->assertNotEmpty($calculate->getOperation());
    }

    public function testNotEmptySetArgument1(): void
    {
        $calculate = new Calculator();
        $calculate->setArgument1(2.5);
        $this->assertNotEmpty($calculate->getArgument1());
    }

    public function testNotEmptySetArgument2(): void
    {
        $calculate = new Calculator();
        $calculate->setArgument2(3.0);
        $this->assertNotEmpty($calculate->getArgument2());
    }


    public function testFloat (): void
    {
        $calculate = new Calculator();
        $calculate->setArgument1(1.5);
        $calculate->setArgument2(4);

        $this->assertIsFloat($calculate->getArgument1());
        $this->assertIsFloat($calculate->getArgument2());
    }

    public function testOperations(): void
    {
        $calculate = new Calculator();
        $calculate->setOperation("+");
        $allowedOperations = ["+", "-", "*", "/"];

        $this->assertTrue(in_array($calculate->getOperation(), $allowedOperations));
    }
}
