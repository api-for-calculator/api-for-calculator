<?php

namespace App\Tests;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;

class SwaggerApiTest extends ApiTestCase
{
    public function testApi(): void
    {
        $response = static::createClient()->request('GET', '/api');

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(['@id' => '/api']);
    }
}
